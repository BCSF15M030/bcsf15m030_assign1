/*	
**	This program is a template for SP lab 3 task 4 you are  
**	required to implement toHexadecimal functions.
**	the program uses bitwise operators to compute bit set
**	and number system conversion
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
**	function takes one integer variable and convert it into hexadecimal
**	and print the converted number on stdout. 
*/
void toHexadecimal(unsigned int);

int main()
{
	int num;
	printf("Enter the number to convert : ");		/*	taking input to convert
	
	//Call function toHexadecimal from here*/
	scanf("%d", &num);
	toHexadecimal(num);
	return 0;
}

void toHexadecimal(unsigned int num)
{
	//WRITE YOUR CODE HERE
	char *output = malloc(sizeof(unsigned) * 2 + 3);
        strcpy(output, "0x00000000");
	
    	static char hex[] = "0123456789ABCDEF";

    	// represents the end of the string.
    	int index = 9;
	int i =9;
    	while (num > 0 ) 
	{
        	output[index--] = hex[(num & 0xF)];
        	num >>= 4;             
    	}
	printf("Hexa Eqivalent  : ");
	printf("%s\n",output);
}
