The program is template for task 1 of lab 3 that students are required to
complete in a lab.

Write a C program in which main() calls char* read_line() function that reads a line from stdin using getchar() and return pointer to string.
After receiving back the address of the line entered by the user, the  main() function calls another function  char** tokenize(char* line),
Which is passed the input string and it will return address of a  2D character array created on heap. 
You can assume that the maximum number of tokens in the input string can be 7 and each token will not occupy more than 14 characters including the terminating NULL character.

The main function is written they are required to complete the code of following functions.

1)	char** tokenize(char* line)
2)	char* read_line()
