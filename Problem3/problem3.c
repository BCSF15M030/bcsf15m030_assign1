/*
**	This program is a template for SP lab 3 task 4 you are  
**	required to complete the logic for tokenizing the string.
*/

#include <stdio.h>
#include <string.h>


int main()
{
        /*
        **      char array to keep input string size is 100 the string contain 
        **      7 word each of size not more than 13 so 7*14=98
        */
        char line[100];
        printf("Enter the line to tokenize\n");
        fgets(line, 100, stdin);
	int length=strlen(line);
	line[length-1]=NULL;
        char arr[8][14]={0};

        /*Add your code here */

        char* ptr;
        int i=0;
        ptr = strtok(line, " ");
	int j=0;
        while(ptr !=NULL){

                strcpy(arr[i++],ptr);
                ptr = strtok(NULL, " ");
		j++;
        }
        for(i=0;i<j;i++){
                printf("%s ",arr[i]);
        }
	printf("\n");

        return 0;
}
