The program is template for 3rd task of lab 3 that students are required to
complete in a lab.

Write a C program in which main() function reads a line of words from stdin using fgets() function. 
You are required to convert the inputted string into a 2d array of characters.
Each word on separate array and null terminated.
Assume that the user will not enter more than seven words with no word of more than 13 characters. 

The main function contains the code for inputting string you are required to write a logic for tokenizing it.
