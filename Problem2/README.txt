The program is template for 2nd task of lab 3 that students are required to
complete in a lab.

Write a C program in which the main() function reads a file containing information about two matrices. 
Use fopen() to open and fscanf() function to read the file. 
Use malloc() to assign appropriate memory memory for the two input matrices, and the output matrix.

You are required to complete the main function.
